nnoremap <silent> <leader><CR> :MarkdownPreview<CR>
nnoremap <C-C> :MarkdownPreviewStop<CR>
let g:mkdp_page_title = '「${name}」'
let g:mkdp_auto_start = 1

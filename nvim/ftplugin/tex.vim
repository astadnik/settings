let g:vimtex_compiler_progname='nvr'
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
let g:tex_conceal='abdmgs' 
let g:vimtex_fold_enabled=1
setlocal spell
set spelllang=en_gb,uk,ru
map <leader>r :call UltiSnips#RefreshSnippets()<CR>
" Inkscape figures
inoremap <C-f> <Esc>: silent exec '.!inkscape-figures create "'.getline('.').'" "'.b:vimtex.root.'/figures/"'<CR><CR>:w<CR>
nnoremap <C-f> : silent exec '!inkscape-figures edit "'.b:vimtex.root.'/figures/" > /dev/null 2>&1 &'<CR><CR>:redraw!<CR>

local nvim_lsp = require('lspconfig')
local map = vim.api.nvim_set_keymap

-- Use an on_attach function to only map the following keys after the language
-- server attaches to the current buffer
local on_attach = function(_, bufnr)
    local function buf_set_keymap(...)
        vim.api.nvim_buf_set_keymap(bufnr, ...)
    end
    -- local function buf_set_option(...)
    --     vim.api.nvim_buf_set_option(bufnr, ...)
    -- end

    -- Enable completion triggered by <c-x><c-o> buf_set_option('omnifunc',
    -- 'v:lua.vim.lsp.omnifunc')

    -- Mappings.
    local opts = {noremap = true, silent = true}

    -- See `:help vim.lsp.*` for documentation on any of the below functions
    buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
    -- buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
    buf_set_keymap('n', '<leader>k', '<cmd>lua vim.lsp.buf.signature_help()<CR>',
                   opts)
    buf_set_keymap('n', '<space>wa',
                   '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '<space>wr',
                   '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '<space>wl',
                   '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>',
                   opts)
    buf_set_keymap('n', '<space>D',
                   '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
    buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
    buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>',
                   opts)
    buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
    buf_set_keymap('n', '<space>e',
                   '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>',
                   opts)
    buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>',
                   opts)
    buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>',
                   opts)
    -- buf_set_keymap('n', '<space>q',
    --                '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
    buf_set_keymap("n", "<space>f", "<cmd>lua vim.lsp.buf.formatting()<CR>",
                   opts)

end

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

-- Use a loop to conveniently call 'setup' on multiple servers and map buffer
-- local keybindings when the language server attaches
local servers = {"pyright", "rust_analyzer", "clangd"}
for _, lsp in ipairs(servers) do
    nvim_lsp[lsp].setup {
        on_attach = on_attach,
        flags = {debounce_text_changes = 150},
        capabilities = capabilities
    }
end

-- Ultisnips

vim.g.UltiSnipsExpandTrigger = "<C-l>"
-- vim.g.UltiSnipsListSnippets="<C-n>"
vim.g.UltiSnipsJumpForwardTrigger = "<C-l>"
vim.g.UltiSnipsJumpBackwardTrigger = "<C-h>"
vim.g.ultisnips_python_style = 'google'
vim.g.snips_author = 'astadnik'

------------- Compe  -- -----------

require'compe'.setup {
    enabled = true,
    autocomplete = true,
    debug = false,
    min_length = 1,
    preselect = 'enable',
    throttle_time = 80,
    source_timeout = 200,
    incomplete_delay = 400,
    max_abbr_width = 100,
    max_kind_width = 100,
    max_menu_width = 100,
    documentation = true,
    source = {
        path = true,
        nvim_lsp = true,
        ultisnips = true,
        tmux = {all_panes = true, ultisnips = true},
        tabnine = true
    }
}

map("i", "<C-space>", "compe#complete()", {noremap = true, expr = true})
map("i", "<C-L>", "compe#confirm({ 'keys': '<CR>', 'select': v:true })",
    {noremap = true, expr = true})
map("i", "<C-e>", "compe#close('<C-e>')", {noremap = true, expr = true})
map('i', '<silent><expr>', '<C-f>     compe#scroll({ "delta": +4 })',
    {noremap = true})
map('i', '<silent><expr>', '<C-d>     compe#scroll({ "delta": -4 })',
    {noremap = true})
-- map("i", "<S-Tab>", "v:lua.s_tab_complete()", {noremap = true, expr = true})
-- map("s", "<S-Tab>", "v:lua.s_tab_complete()", {noremap = true, expr = true})

-- Make runtime files discoverable to the server
local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, 'lua/?.lua')
table.insert(runtime_path, 'lua/?/init.lua')

require'lspconfig'.sumneko_lua.setup {
    cmd = {"/usr/bin/lua-language-server"},
    on_attach = on_attach,
    capabilities = capabilities,
    settings = {
        Lua = {
            runtime = {
                -- Tell the language server which version of Lua you're using (most
                -- likely LuaJIT in the case of Neovim)
                version = 'LuaJIT',
                -- Setup your lua path
                path = runtime_path
            },
            diagnostics = {
                -- Get the language server to recognize the `vim` global
                globals = {'vim'}
            },
            workspace = {
                -- Make the server aware of Neovim runtime files
                library = vim.api.nvim_get_runtime_file("", true)
            },
            -- Do not send telemetry data containing a randomized but unique
            -- identifier
            telemetry = {enable = false}
        }
    }
}

require"lsp_signature".setup() -- Note: add in lsp client on-attach
require'lspconfig'.vimls.setup {}
require'lspkind'.init()
require("trouble").setup {}
map("n", "<leader>q", "<cmd>Trouble lsp_workspace_diagnostics<cr>",
    {silent = true, noremap = true})

# Build container
docker build --build-arg UID=$(id -u) --build-arg GID=$(id -g) -t astadnik/astadnik_setup .

# Run container
docker run -itv /home/astadnik/projects:/home/astadnik/projects --gpus all --name astadnik_setup --net=host --env="DISPLAY" astadnik/astadnik_setup

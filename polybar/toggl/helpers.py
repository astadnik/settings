"""Helpers functions"""

import argparse

def parse():
    """Parse arguments

    """
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()

    group.add_argument('-n', '--new', type=str,
                       help="Create a new Toggl entry.")
    group.add_argument('-s', '--stop', action='store_true',
                       help="Stop the running Toggl entry.")
    group.add_argument('-r', '--resume', action='store_true',
                       help="Resume a previous Toggl entry.")
    group.add_argument('-a', '--add', type=str,
                       help="Add a Toggl entry.")

    parser.add_argument('-d', '--duration',
                        help="Set duration for a done entry.")
    parser.add_argument('-t', '--tag', nargs='+',
                        help="Set tags for the new Toggl entry.")

    parser.add_argument('--running', default=False,
                        help="Check running Toggl entry.",
                        action='store_true')

    parser.add_argument("-w", "--watch", action='store_true',
                        help="Watch the current toggl progress")
    parser.add_argument("-i", "--interval", type=int, default=5,
                        help="Interval for the watch")

    args = parser.parse_args()
    return args

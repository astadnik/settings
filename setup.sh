ln -s ~/settings/autorandr ~/.config/autorandr
ln -s ~/settings/nvim ~/.config/nvim
ln -s ~/settings/.docker ~/.config/.docker
ln -s ~/settings/dunst ~/.config/dunst
ln -s ~/settings/i3/ ~/.config/i3
ln -s ~/settings/.p10k.zsh ~/.p10k.zsh
ln -s ~/settings/sway ~/.config/sway
# ln -s ~/settings/variety/scripts ~/.config/variety/scripts
ln -s ~/settings/wal ~/.config/wal
ln -s ~/settings/ranger ~/.config/ranger
ln -s ~/settings/.zshrc ~/.zshrc
ln -s ~/settings/.zshenv ~/.zshenv
ln -s ~/settings/.gitconfig ~/.gitconfig
ln -s ~/settings/.tmux.conf ~/.tmux.conf
ln -s ~/settings/bin/* ~/.local/bin
ln -s ~/settings/nnn ~/.config/nnn
